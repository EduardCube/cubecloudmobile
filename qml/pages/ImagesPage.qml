import QtQuick 2.13
import QtQuick.Controls 2.13
import Felgo 3.0

Page
{
    id: page

        GridView
        {

            id: gridImages

            anchors.topMargin: dp(5)
            anchors.fill: parent
            cellWidth: (parent.width/3)
            cellHeight: cellWidth
            clip: true
            boundsBehavior: Flickable.OvershootBounds // for flicking
            snapMode: GridView.SnapToRow // to stop at begin of item

            Component
            {
                id: detailPageComponent
                ImagesDetailPage{}
            }

            model: JsonListModel
            {
                id: listModel
                source: dataModel.images
                fields: ["img_name", "thumb_url", "model"]
            }

            delegate: Item
            {
                id: imageDelagate

                property var isCurrent: GridView.isCurrentItem

                height: gridImages.cellHeight
                width: gridImages.cellWidth

                Image
                {
                    id: image
                    fillMode: Image.PreserveAspectCrop
                    anchors
                    {
                        rightMargin:
                        {
                            (index + 1) % 3 === 0 ? 0 : 3
                        }
                        bottomMargin: dp(20)
                        fill: parent
                    }
                    source: thumb_url
                }

                Text
                {
                    anchors
                    {
                        horizontalCenter: parent.horizontalCenter
                        top: image.bottom
                        topMargin: dp(2)
                        bottomMargin: dp(3)
                    }
                    font.family: "Tahoma"
                    renderType: Text.QtRendering
                    text: img_name
                    font.pointSize: sp(9)
                }

            }
            footer: VisibilityRefreshHandler
            {
                visible:  dataModel.numImagesGetInTotal < dataModel.numTotalImages           
            }
            onMovementEnded:
            {
                console.log("atYEnd = ", atYEnd)
                if(atYEnd && dataModel.numImagesGetInTotal < dataModel.numTotalImages)
                {
                     logic.getImages(cellWidth)
                }
            }

            ScrollIndicator
            {
                flickable:  gridImages
            }

        }

}

