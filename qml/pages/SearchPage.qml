import Felgo 3.0
import QtQuick 2.0

Page
{
    id: searchPage
    title: qsTr("Main");

    rightBarItem: NavigationBarRow
    {
        ActivityIndicatorBarItem
        {
            animating: false
        }
    }
        Column
        {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: contentPadding
            spacing: contentPadding

            Row
            {
                spacing: contentPadding

                AppButton
                {
                    text: qsTr("Go")
                    onClicked: logic.getImages(searchPage.width / 3)
                }

            }
        }

        function showImages ()
        {
            if(navigationStack.depth === 1)
            {
                navigationStack.popAllExceptFirstAndPush(gridPageComponent)               
            }
        }

        Component
        {
            id: gridPageComponent
            ImagesPage {}
        }

        Connections
        {
            target: dataModel
            onListingsReceived: showImages()
        }
    }

