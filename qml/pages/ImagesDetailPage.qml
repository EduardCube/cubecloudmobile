import QtQuick 2.0
import Felgo 3.0

Page
{

    property var model: ({})
    title: "Details"

    rightBarItem: IconButtonBarItem
    {
        icon: IconType.heart
        onClicked:
        {

        }
    }

    clip: true

    Flickable
    {
        id: scroll
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: contentCol.height + contentPadding
        bottomMargin: contentPadding

        Column
        {
            id: contentCol
            y: contentPadding
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: contentPadding
            spacing: contentPadding

            AppImage
            {
                source: model.img_url
                width: parent.width
                height: model && width * model.img_height / model.img_width || 0
                anchors.horizontalCenter: parent.horizontalCenter
            }

        }

        ScrollIndicator
        {
            flickable:  scroll
        }
    }
}
