import QtQuick 2.13
import Felgo 3.0
import Qt.labs.settings 1.1

Item
{

    property alias dispatcher: logicConnection.target
    signal listingsReceived;

    readonly property var images: _img.createImagesModel(_img.images)
    readonly property int numImagesGetInTotal: images.length
    readonly property int numTotalPages: _img.numTotalPages
    readonly property int numTotalImages: _img.numTotalImages
    readonly property int currentPage: _img.currentPage

    ImagesClient
    {
        id: imageClient
    }

    Connections
    {
        id: logicConnection

        onGetImages:
        {
           imageClient.getImages(_img.currentPage, _img.responseGetImagesCallback, imgWidthHeight)
        }

    }

    Item
    {
        id:  _img

        property var images: []
        property int numTotalPages
        property int currentPage: 0
        property int numImagesGet
        property int numTotalImages

        function responseGetImagesCallback(obj)
        {
            images = images.concat(obj.images)
            numTotalPages = obj.total_pages || 0
            numImagesGet = obj.images.length
            numTotalImages  = obj.total_images || 0
            if (numImagesGet !==0)
            {
                ++currentPage
                listingsReceived()
            }
            console.log("images received", obj.num_of_img_received || 0)
        }

        function createImagesModel(source)
        {
            return source.map(function(data)
            {
                return { //todo add img extension, img data
                    img_name: data.img_name,
                    thumb_url: data.thumb_url,
                    model: data
                }
            })
        }

        function sendImagesResponseCallback(obj)
        {
            var statusCode = obj.status
            console.debug("Server returned app code : ", statusCode)

            if(statusCode === "ok")
            {
               console.debug("Successfully sent image")
            }
            else
            {
                console.debug("Error in sending image")
            }

        }

    }

}
