import QtQuick 2.13
import Felgo 3.0

Item
{

    Component.onCompleted:
    {
        HttpNetworkActivityIndicator.activationDelay = 0
    }

    Item
    {
        id: _
        readonly property string imagesURL: "http://localhost:3000/images/"     

        function buildUrl (paramMap)
        {
            var url = imagesURL
            paramMap.forEach(element => url += element +  "/");
            return url
        }


        function getImagesRequest (page, callback, imgWidthHeight)
        {
            var paramMap  = [page, imgWidthHeight, imgWidthHeight]
            var url = buildUrl(paramMap)
            HttpRequest
            .get(url)
            .then(function(res)
            {
                var content = res.text
                try
                {
                    var obj = JSON.parse(content)
                    console.log(res.status);
                    console.log(JSON.stringify(res.header, null, 4));
                    console.log(JSON.stringify(res.body, null, 4));
                }
                catch(ex)
                {
                    console.error("Could not parse JSON: ", ex)
                    return
                }
                console.debug("Success parsing JSON")
                callback(obj)
            })
            .catch(function(err)
            {
                console.debug("Fatal error in URL GET ", err.code)
            })
        }

    }
    function getImages (page, callback, imgWidthHeight)
    {
        _.getImagesRequest(page, callback, imgWidthHeight)
    }


    function sendImages(callback)
    {
        HttpRequest
        .post(_.imagesURL)
        .attach('image1', '/home/eduard/Pictures/road.png')
        .then(function(res)
        {
            var content = res.text
            try
            {
                var obj = JSON.parse(content)
                console.log(res.status);
                console.log(JSON.stringify(res.header, null, 4));
                console.log(JSON.stringify(res.body, null, 4));
            }
            catch(ex)
            {
                console.error("Could not parse JSON: ", ex)
                return
            }
            console.debug("Success parsing JSON")
            callback(obj)
        })
        .catch(function(err)
        {
            console.debug("Fatal error in URL POST ", err.code)
        })

    }

}
