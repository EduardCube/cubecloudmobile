import QtQuick 2.13
import Felgo 3.0

Item
{
    signal getImages(var imgWidthHeight)
    signal showRecentSearchs()
    signal sendImages()
    signal downloadImage()
}
